
// Salary
// CJ VanDenEng

#include <iostream>
#include <conio.h>

using namespace std;

struct Data{
	int id;
	string firstname;
	string lastname;
	double payrate;
	int hours;
};

int main()
{
	double total = 0;
	const int num_people = 5;	 
	Data data[num_people];
	for (int i = 0; i < num_people; i++) {
		cout << "Person " << (i + 1) << ":\n";
		cout << "id: ";
		cin >> data[i].id;
		cout << "first name: ";
		cin >> data[i].firstname;
		cout << "last name: ";
		cin >> data[i].lastname;
		cout << "payrate: ";
		cin >> data[i].payrate;
		cout << "hours: ";
		cin >> data[i].hours;
		cout << "\n";
	}
	for (int i = 0; i < num_people; i++) {
		string name = data[i].firstname + " " + data[i].lastname;
		double grosspay = data[i].payrate * data[i].hours;
		cout << "person " << (i + 1) << ":\nid: " << data[i].id << "\nName: " << name << "\nGross Pay: " << grosspay << "\n\n";
		total = total + grosspay;
		if (i == num_people - 1) {
			cout << "Total Pay for all employees: " << total;
		}
		
	}
	

	(void)_getch();
	return 0;
}
